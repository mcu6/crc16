
#ifndef _CRC16_H_
#define _CRC16_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _CRC16_C_
#define CRC16_EXT extern
#else 
#define CRC16_EXT
#endif

#include <stdint.h>
#include "crc16.h"
   
typedef union {
  uint8_t  crc[2];
  uint16_t crc16;
}CRC_Check;
 
CRC16_EXT uint16_t CRC16_2(uint8_t *pchMsg, uint8_t wDataLen);  //CRC16 校验 函数声明

#ifdef __cplusplus
}
#endif

#endif
