# export CMAKE_INCLUDE_PATH=/tmp/usr/include/hello:$CMAKE_INCLUDE_PATH 
#FIND_PATH(PM_LICENSE_INCLUDE_DIR hello.h)  
# or

#FIND_PATH(PM_LICENSE_INCLUDE_DIR hello.h  /usr/include/hello  
#	/usr/local/include/hello  /tmp/usr/include/hello )

#FIND_PATH(PM_LICENSE_INCLUDE_DIR pm_license.h  /usr/local/include)
# FIND_PATH(PM_LICENSE_INCLUDE_DIR serial_number.hpp pm_license.h PATHS  "${CMAKE_INSTALL_PREFIX}/include")
FIND_PATH(CRC16_INCLUDE_DIR crc16.h PATHS  "${CMAKE_INSTALL_PREFIX}/include")

# export CMAKE_LIBRARY_PATH=/tmp/usr/lib/:$CMAKE_LIBRARY_PATH
# FIND_LIBRARY(PM_LICENSE_LIBRARY  hello )
# or
#FIND_LIBRARY(PM_LICENSE_LIBRARY  hello  /usr/lib/usr/local/lib  /tmp/usr/lib)
#FIND_LIBRARY(PM_LICENSE_LIBRARY  license  /usr/local/lib/  )
#FIND_LIBRARY(PM_LICENSE_LIBRARY  license  PATHS  "${CMAKE_INSTALL_PREFIX}/lib"  )
# FIND_LIBRARY(PM_SERIAL_NUMBER_LIBRARY serial_number  PATHS  "${CMAKE_INSTALL_PREFIX}/lib"  )
# FIND_LIBRARY(PM_LICENSE_LIBRARY license  PATHS  "${CMAKE_INSTALL_PREFIX}/lib"  )
FIND_LIBRARY(CRC16_LIBRARY crc16  PATHS  "${CMAKE_INSTALL_PREFIX}/lib" )

IF (CRC16_INCLUDE_DIR AND CRC16_LIBRARY)
	SET(CRC16_FOUND TRUE)
ENDIF (CRC16_INCLUDE_DIR AND CRC16_LIBRARY)

IF (CRC16_FOUND)
	IF (NOT CRC16_FIND_QUIETLY)
		MESSAGE(STATUS "Found crc16.h Path: ${CRC16_INCLUDE_DIR}")
		MESSAGE(STATUS "Found lbcrc16: ${CRC16_LIBRARY}")
	ENDIF (NOT CRC16_FIND_QUIETLY)
ELSE (CRC16_FOUND)
	IF (CRC16_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find license library")
	ENDIF (CRC16_FIND_REQUIRED)
ENDIF (CRC16_FOUND)
